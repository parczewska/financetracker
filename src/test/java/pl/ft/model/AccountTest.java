package pl.ft.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountTest {

    private Account account = new Account();

    @Test
    void when_add_one_new_user_then_return_one_user_in_account() {
        User user = new User("Jan", "Nowak");
        int expected = 0;
        int result = account.getSize();
        assertEquals(expected, result);
        account.addUser(user);
        expected = 1;
        result = account.getSize();
        assertEquals(expected, result);
    }

    @Test
    void when_remove_one_user_then_return_empty_account() {
        User user = new User("Jan", "Nowak");
        account.addUser(user);
        int expected = 1;
        int result = account.getSize();
        assertEquals(expected, result);
        account.removeUser(user);
        expected = 0;
        result = account.getSize();
        assertEquals(expected, result);
    }
}
