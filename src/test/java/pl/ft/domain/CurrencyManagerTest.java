package pl.ft.domain;

import org.junit.jupiter.api.Test;
import pl.ft.model.Money;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CurrencyManagerTest {

    @Test
    void when_given_100_PLN_then_432_USD_as_result() {
        double expected = 432;
        double result = new CurrencyManager(new Money(100)).toUSD();
        assertEquals(expected, result);
    }

    @Test
    void when_given_100_PLN_then_473_EUR_as_result() {
        double ecpected = 473;
        double result = new CurrencyManager(new Money(100)).toEUR();
        assertEquals(ecpected, result);
    }

    @Test
    void when_given_100_PLN_then_570_GBP_as_result() {
        double expected = 570;
        double result = new CurrencyManager(new Money(100)).toGBP();
        assertEquals(expected, result);
    }
}
