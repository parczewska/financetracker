package pl.ft.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.ft.model.Expense;
import pl.ft.model.ExpenseType;
import pl.ft.model.Money;
import pl.ft.model.User;

public class ExpenseTypeManagerTest {

    private static User user = new User("Anna", "Nowak");
    private static ExpenseManager expenseManager = new ExpenseManager(user);
    private static ExpenseTypeManager expenseTypeManager = new ExpenseTypeManager(user);

    @BeforeAll
    static void init() {
        expenseManager.add(new Expense(20, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(150, ExpenseType.CLOTHES));
        expenseManager.add(new Expense(70, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(20, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(150, ExpenseType.CLOTHES));
        expenseManager.add(new Expense(70, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(20, ExpenseType.FOOD));
        expenseManager.add(new Expense(150, ExpenseType.FOOD));
        expenseManager.add(new Expense(70, ExpenseType.BEAUTY));
    }

    @Test
    void when_given_the_food_type_then_return_170() {
        Money expected = new Money(170);
        Money result = expenseTypeManager.calculateExpenseByTYpe(ExpenseType.FOOD);
        assertEquals(expected, result);
    }

    @Test
    void when_given_the_beauty_type_then_return_250() {
        Money expected = new Money(250);
        Money result = expenseTypeManager.calculateExpenseByTYpe(ExpenseType.BEAUTY);
        assertEquals(expected, result);
    }

    @Test
    void when_given_the_clothes_type_then_return_300() {
        Money expected = new Money(300);
        Money result = expenseTypeManager.calculateExpenseByTYpe(ExpenseType.CLOTHES);
        assertEquals(expected, result);
    }
}
