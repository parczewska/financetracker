package pl.ft.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.ft.model.Money;
import pl.ft.model.Payment;
import pl.ft.model.User;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.Year;

public class PaymentManagerTest {

    private static User user = new User("Anna", "Nowak");
    private static PaymentManager paymentManager = new PaymentManager(user);

    @BeforeAll
    static void init() {
        paymentManager.add(new Payment(100));
        paymentManager.add(new Payment(22));
        paymentManager.add(new Payment(30));
        paymentManager.add(new Payment(55));
        paymentManager.add(new Payment(2));
        paymentManager.add(new Payment(15));
        paymentManager.add(new Payment(45));
    }

    @Test
    void when_given_payment_calculate_by_day() {
        Money expected = new Money(269);
        Money result = paymentManager.calculateAllPaymentByDay(MonthDay.now());
        assertEquals(expected, result);
    }

    @Test
    void when_given_payment_calculate_by_month() {
        Money expected = new Money(269);
        Money result = paymentManager.calculateAllPaymentByMonth(LocalDate.now().getMonth());
        assertEquals(expected, result);
    }

    @Test
    void when_given_payment_calculate_by_year() {
        Money expected = new Money(269);
        Money result = paymentManager.calculationAllPaymentByYear(Year.now());
        assertEquals(expected, result);
    }
}
