package pl.ft.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.ft.model.*;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FrugalityManagerTest {

    private static User user = new User("Anna", "Nowak");
    private static PaymentManager paymentManager = new PaymentManager(user);
    private static ExpenseManager expenseManager = new ExpenseManager(user);
    private static FrugalityManager frugalityManager = new FrugalityManager(user);

    @BeforeAll
    static void init() {
        paymentManager.add(new Payment(100));
        paymentManager.add(new Payment(200));
        paymentManager.add(new Payment(30));
        expenseManager.add(new Expense(25, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(10, ExpenseType.CLOTHES));
        expenseManager.add(new Expense(50, ExpenseType.CLOTHES));
    }

    @Test
    void when_given_payment_and_expenses_return_frugality_by_month() {
        Money expected = new Money(245);
        Money result = frugalityManager.calculationFrugalityByMonth(LocalDate.now().getMonth());
        assertEquals(expected, result);
    }

    @Test
    void when_given_payment_and_expenses_return_frugality_by_year() {
        Money expected = new Money(245);
        Money result = frugalityManager.calculationFrugalityByYear(Year.now());
        assertEquals(expected, result);
    }

    @Test
    void when_given_payment_and_expenses_return_frugality_by_three_months() {
        Money expected = new Money(245);
        Money result = frugalityManager.calculationFrugalityByThreeMonths(LocalDate.now().getMonth());
        assertEquals(expected, result);
    }
}
