package pl.ft.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.ft.model.Expense;
import pl.ft.model.ExpenseType;
import pl.ft.model.Money;
import pl.ft.model.User;

import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.time.Year;

public class ExpenseManagerTest {

    private static User user = new User("Anna", "Nowak");
    private static ExpenseManager expenseManager = new ExpenseManager(user);

    @BeforeAll
    static void init() {
        expenseManager.add(new Expense(20, ExpenseType.FOOD));
        expenseManager.add(new Expense(12, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(45, ExpenseType.CLOTHES));
        expenseManager.add(new Expense(20, ExpenseType.FOOD));
        expenseManager.add(new Expense(12, ExpenseType.BEAUTY));
        expenseManager.add(new Expense(45, ExpenseType.CLOTHES));
    }

    @Test
    void when_given_multi_expenses_then_resul_calculate_by_day() {
        Money expected = new Money(154);
        Money result = expenseManager.calculationAllExpensesByDay(MonthDay.now());
        assertEquals(expected, result);
    }

    @Test
    void when_given_multi_expenses_then_result_calculate_by_month() {
        Money expected = new Money(154);
        Money result = expenseManager.calculationAllExpensesByMonth(LocalDate.now().getMonth());
        assertEquals(expected, result);
    }

    @Test
    void when_given_multi_expenses_then_result_calculate_by_year() {
        Money expected = new Money(154);
        Money result = expenseManager.calculationAllExpenseByYear(Year.now());
        assertEquals(expected, result);
    }
}
