package pl.ft.model;

import java.util.ArrayList;
import java.util.List;

public class Account {

    private List<User> users = new ArrayList<>();

    public void addUser(User user) {
        users.add(user);
    }

    public void removeUser(User user) {
        users.remove(user);
    }

    private String getUsersAsString() {

        StringBuilder stringBuilder = new StringBuilder();
        for (User user : users) {

            stringBuilder.append(user);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return getUsersAsString();
    }

    int getSize() {
        return users.size();
    }
}
