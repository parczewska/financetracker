package pl.ft.model;

public enum Currency {

    PLN(1.00),
    USD(4.32),
    EUR(4.73),
    GBP(5.70);

    private double exchange;

    Currency(double exchange) {
        this.exchange = exchange;
    }

    public double getExchange() {
        return exchange;
    }
}
