package pl.ft.model;

public enum ExpenseType {

    FOOD("jedzenie"), MEDICINES("lekarstwa"), CLOTHES("ubrania"), HOBBY("hobby"), TRAVEL("podróże"),
    BEAUTY("uroda");

    private final String name;

    ExpenseType(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }
}
