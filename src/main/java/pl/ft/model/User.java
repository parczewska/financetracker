package pl.ft.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class User {

    private List<Expense> expenses;
    private List<Payment> payments;
    private String name;
    private String lastname;


    public User(String name, String lastname) {

        expenses = new ArrayList<>();
        payments = new ArrayList<>();
        this.name = name;
        this.lastname = lastname;
    }

    public void addExpense(Expense expense) {
        expenses.add(expense);
    }

    public void addPayments(Payment payment) {
        payments.add(payment);
    }

    @Override
    public String toString() {
        return name + " " + lastname + "\nwydatki:\n" + getExpensesAsString() + "\nwpłaty:\n" + getPaymentsAsString();
    }

    private String getExpensesAsString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < expenses.size(); i++) {
            stringBuilder.append(expenses.get(i));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private String getPaymentsAsString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < payments.size(); i++) {
            stringBuilder.append(payments.get(i));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public Money calculatePayments() {
        double sumPayments = 0;

        for (int i = 0; i < payments.size(); i++) {
            sumPayments += payments.get(i).getAmount();
        }
        return new Money(sumPayments);
    }

    public Money calculateExpenses() {
        double sumExpenses = 0;

        for (int i = 0; i < expenses.size(); i++) {
            sumExpenses += expenses.get(i).getAmount();
        }
        return new Money(sumExpenses);
    }

    public List<Expense> getExpenses() {
        return Collections.unmodifiableList(expenses);

    }

    public List<Payment> getPayments() {
        return Collections.unmodifiableList(payments);
    }

    public Money calculateGain() {
        double allPayments = calculatePayments().getAmount();
        double allExpenses = calculateExpenses().getAmount();
        return new Money(allPayments - allExpenses);
    }
}
