package pl.ft.model;

import java.time.LocalDateTime;

public class Expense {

    private Money money;
    private ExpenseType type;
    private LocalDateTime localDateTime;


    public Expense(double money, ExpenseType category) {
        this.money = new Money(money);
        this.type = category;
        localDateTime = LocalDateTime.now();
    }

    public Expense(double money, ExpenseType category, LocalDateTime localDateTime) {
        this.money = new Money(money);
        this.type = category;
        this.localDateTime = localDateTime;
    }

    public double getAmount() {
        return money.getAmount();
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public ExpenseType getType() {
        return type;
    }

    @Override
    public String toString() {
        return money.getAmount() + ", " + localDateTime + " " + type.getName();
    }

}
