package pl.ft.model;

import pl.ft.model.Money;

import java.time.LocalDateTime;

public class Payment {

    private Money money;
    private LocalDateTime localDateTime;

    public Payment(double money) {
        this.money = new Money(money);
        localDateTime = LocalDateTime.now();
    }

    public Payment(double money, LocalDateTime localDateTime) {
        this.money = new Money(money);
        this.localDateTime = localDateTime;
    }

    public double getAmount() {
        return money.getAmount();
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    @Override
    public String toString() {
        return money.getAmount() + ", " + localDateTime + " ";
    }
}
