package pl.ft.domain;

import pl.ft.model.Money;
import pl.ft.model.User;

import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

public class FrugalityManager {

    private final User user;

    public FrugalityManager(User user) {
        this.user = user;
    }

    public Money calculationFrugalityByMonth(Month month) {

        PaymentManager paymentManager = new PaymentManager(user);
        Money paymentMoney = paymentManager.calculateAllPaymentByMonth(month);

        ExpenseManager expenseManager = new ExpenseManager(user);
        Money expenseMoney = expenseManager.calculationAllExpensesByMonth(month);

        return new Money(paymentMoney.getAmount() - expenseMoney.getAmount());
    }

    public Money calculationFrugalityByYear(Year year) {

        PaymentManager paymentManager = new PaymentManager(user);
        Money paymentMoney = paymentManager.calculationAllPaymentByYear(year);

        ExpenseManager expenseManager = new ExpenseManager(user);
        Money expenseMoney = expenseManager.calculationAllExpenseByYear(year);

        return new Money(paymentMoney.getAmount() - expenseMoney.getAmount());
    }

    public Money calculationFrugalityByThreeMonths(Month month) {

        List<Money> expensesMoney = calculateExpensesByThreeMonth(month);
        List<Money> paymentsMoney = calculatePaymentsByThreeMonth(month);
        double expenses = calculateMoney(expensesMoney);
        double payments = calculateMoney(paymentsMoney);

        return new Money(payments - expenses);
    }

    private List<Money> calculatePaymentsByThreeMonth(Month month) {

        PaymentManager paymentManager = new PaymentManager(user);

        List<Money> paymentMoneyList = new ArrayList<>();
        paymentMoneyList.add(paymentManager.calculateAllPaymentByMonth(month));
        paymentMoneyList.add(paymentManager.calculateAllPaymentByMonth(month.minus(1)));
        paymentMoneyList.add(paymentManager.calculateAllPaymentByMonth(month.minus(2)));

        return paymentMoneyList;
    }

    private List<Money> calculateExpensesByThreeMonth(Month month) {

        ExpenseManager expenseManager = new ExpenseManager(user);

        List<Money> expenseMoneyList = new ArrayList<>();
        expenseMoneyList.add(expenseManager.calculationAllExpensesByMonth(month));
        expenseMoneyList.add(expenseManager.calculationAllExpensesByMonth(month.minus(1)));
        expenseMoneyList.add(expenseManager.calculationAllExpensesByMonth(month.minus(2)));

        return expenseMoneyList;
    }

    private double calculateMoney(List<Money> transactions) {

        double result = 0;

        for (Money money : transactions) {
            result += money.getAmount();
        }

        return result;
    }
}
