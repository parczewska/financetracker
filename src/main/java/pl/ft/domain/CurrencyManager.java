package pl.ft.domain;

import pl.ft.model.Currency;
import pl.ft.model.Money;

public class CurrencyManager {

    private Money money;

    public CurrencyManager(Money money) {

        this.money = money;
    }

    public double toUSD() {

        double amount = money.getAmount();
        return Math.round(amount * Currency.USD.getExchange() * 100.00) / 100.00;
    }

    public double toEUR() {

        double amount = money.getAmount();
        return Math.round(amount * Currency.EUR.getExchange() * 100.00) / 100.00;
    }

    public double toGBP() {

        double amount = money.getAmount();
        return Math.round(amount * Currency.GBP.getExchange() * 100.00) / 100.00;
    }
}
