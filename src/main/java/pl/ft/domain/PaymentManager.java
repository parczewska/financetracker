package pl.ft.domain;

import pl.ft.model.Money;
import pl.ft.model.Payment;
import pl.ft.model.User;

import java.time.Month;
import java.time.MonthDay;
import java.time.Year;

public class PaymentManager {

    private User user;

    public PaymentManager(User user) {
        this.user = user;
    }

    public void add(Payment payment) {
        user.addPayments(payment);
    }

    public Money calculateAllPaymentByDay(MonthDay day) {

        double sum = 0;
        for (Payment payment : user.getPayments()) {

            if (payment.getLocalDateTime().getDayOfMonth() == day.getDayOfMonth() &&
                    payment.getLocalDateTime().getMonth() == day.getMonth()) {

                sum += payment.getAmount();
            }
        }
        return new Money(sum);
    }

    public Money calculateAllPaymentByMonth(Month month) {

        double sum = 0;
        for (Payment payment : user.getPayments()) {

            if (payment.getLocalDateTime().getMonth() == month) {
                sum += payment.getAmount();
            }
        }
        return new Money(sum);
    }

    public Money calculationAllPaymentByYear(Year year) {

        double sum = 0;
        for (Payment payment : user.getPayments()) {

            if (payment.getLocalDateTime().getYear() == year.getValue()) {
                sum += payment.getAmount();
            }
        }
        return new Money(sum);
    }

}
