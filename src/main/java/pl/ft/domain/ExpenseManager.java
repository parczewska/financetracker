package pl.ft.domain;

import pl.ft.model.Expense;
import pl.ft.model.Money;
import pl.ft.model.User;

import java.time.Month;
import java.time.MonthDay;
import java.time.Year;

public class ExpenseManager {

    private User user;

    public ExpenseManager(User user) {

        this.user = user;
    }

    public void add(Expense expense) {

        user.addExpense(expense);
    }

    public Money calculationAllExpensesByDay(MonthDay day) {

        double sum = 0;
        for (Expense expense : user.getExpenses()) {
            if (expense.getLocalDateTime().getDayOfMonth() == day.getDayOfMonth() &&
                    expense.getLocalDateTime().getMonth() == day.getMonth()) {

                sum += expense.getAmount();
            }
        }
        return new Money(sum);
    }


    public Money calculationAllExpensesByMonth(Month month) {

        double sum = 0;
        for (Expense expense : user.getExpenses()) {

            if (expense.getLocalDateTime().getMonth() == month) {
                sum += expense.getAmount();
            }
        }
        return new Money(sum);
    }

    public Money calculationAllExpenseByYear(Year year) {

        double sum = 0;
        for (Expense expense : user.getExpenses()) {

            if (expense.getLocalDateTime().getYear() == year.getValue()) {
                sum += expense.getAmount();
            }
        }
        return new Money(sum);
    }
}
