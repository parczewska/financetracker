package pl.ft.domain;

import pl.ft.model.Expense;
import pl.ft.model.ExpenseType;
import pl.ft.model.Money;
import pl.ft.model.User;

public class ExpenseTypeManager {

    private User user;


    public ExpenseTypeManager(User user) {

        this.user = user;
    }

    public Money calculateExpenseByTYpe(ExpenseType expenseType) {

        double sum = 0;
        for (Expense expense : user.getExpenses()) {

            if (expense.getType() == expenseType) {

                sum += expense.getAmount();
            }
        }
        return new Money(sum);
    }
}
